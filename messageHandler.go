package mixer

import (
	"encoding/json"
	"errors"
	"fmt"
)

func (s *Session) handleEvent(conn *Connection, msg []byte) error {
	var event Event

	err := json.Unmarshal(msg, &event)
	if err != nil {
		return err
	}
	//fmt.Println(event.Event)

	switch event.Event {
	case "ChatMessage":
		var messageEvent MessageEvent
		err = json.Unmarshal(msg, &messageEvent)
		if err != nil {
			return err
		}
		message := NewMessage(&messageEvent)
		s.onMessageCreate(conn, message)
	case "UserJoin":
		return nil
	case "DeleteMessage":
		return nil
	}

	return nil
}

func (s *Session) handleReply(conn *Connection, msg []byte) error {
	var reply Reply

	err := json.Unmarshal(msg, &reply)

	if err != nil {
		return err
	}
	//fmt.Println(reply)

	if reply.Error != nil {
		return errors.New(*reply.Error)
	}

	replyType := conn.sock.Chain[reply.ID]
	switch replyType {
	case M_AUTH:
		fmt.Println("Auth")
	case M_MSG:
		fmt.Println("Msg")
		//s.onMessageCreate()
	}

	delete(conn.sock.Chain, reply.ID) // Ensure we don't use too much RAM

	return nil
}
