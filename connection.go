package mixer

import (
	"encoding/json"
)

func (c *Connection) readMessage() ([]byte, error) {
	_, message, err := c.sock.Conn.ReadMessage()

	if err != nil {
		return nil, err
	}

	return message, nil
}

func (c *Connection) writeMessage(method string, arguments []interface{}) error {
	id := c.sock.IncrCounter()

	err := c.sock.Conn.WriteJSON(&Method{
		Id:     id,
		Method: method,
		Args:   arguments,
		T:      "method",
	})

	if err != nil {
		return err
	}

	c.sock.Chain[id] = methodIdFromName(method)

	return nil
}

func (c *Connection) listen(s *Session, x chan interface{}) {
	for {
		//_, ok := <-x
		//if ok {
		message, err := c.readMessage()
		if err != nil {
			s.onError(err)
			continue
		}

		var pre Preload
		err = json.Unmarshal(message, &pre)
		if err != nil {
			s.onError(err)
			continue
		}

		switch pre.T {
		case "event":
			//fmt.Println("event")
			err = s.handleEvent(c, message)
			if err != nil {
				s.onError(err)
				continue
			}
		case "reply":
			//fmt.Println("reply")
			err = s.handleReply(c, message)
			if err != nil {
				s.onError(err)
				continue
			}
		}
	}
}

func (c *Connection) SendMessage(message string) error {
	err := c.writeMessage("msg", []interface{}{message})
	return err
}

func (c *Connection) TimeoutUser(user string, dur string) error {
	err := c.writeMessage("timeout", []interface{}{user, dur})
	return err
}

func (cSock *ConnectionSock) IncrCounter() int {
	cSock.Counter += 1
	return cSock.Counter
}
