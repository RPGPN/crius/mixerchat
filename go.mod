module gitlab.com/RPGPN/crius/mixerchat

go 1.13

require (
	github.com/gorilla/websocket v1.4.2
	github.com/sirupsen/logrus v1.5.0
)
