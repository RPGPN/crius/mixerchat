package mixer

const (
	REST_API = "https://mixer.com/api/v1"

	M_AUTH    = 0
	M_MSG     = 1
	M_WHISPER = 2
	M_TIMEOUT = 3
	M_PING    = 4
)
