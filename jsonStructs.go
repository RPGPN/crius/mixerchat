package mixer

type Method struct {
	T      string        `json:"type"`
	Method string        `json:"method"`
	Args   []interface{} `json:"arguments"`
	Id     int           `json:"id"`
}

type ConnectionJSON struct {
	Endpoints   []string `json:"endpoints,omitempty"`
	Permissions []string `json:"permissions,omitempty"`
	AuthKey     *string  `json:"authkey,omitempty"`
	IsLoadShed  *bool    `json:"isLoadShed,omitempty"`
	Roles       []string `json:"roles,omitempty"`

	Message *string `json:"message,omitempty"`
	Error   *string `json:"error,omitempty"`
}

type Preload struct {
	T string `json:"type"`
}
