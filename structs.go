package mixer

import (
	"github.com/gorilla/websocket"
	"net/http"
)

type Connection struct {
	endpoints   []string
	permissions []string
	authKey     string
	isLoadShed  bool
	roles       []string
	As          int

	sock *ConnectionSock
}

type ConnectionSock struct {
	Conn    *websocket.Conn
	Counter int
	Chain   map[int]int
}

type Session struct {
	token      string
	httpClient *http.Client
	clientID   string
	connectAs  int

	onMessageCreate func(*Connection, *Message)
	onMessageDelete func(*Connection, string)
	onPurge         func(*Connection, int)
	onError         func(error)
	//onUserUpdate func(*Session, )

	//channels map[int]*Connection
	x chan interface{}
}

type User struct {
	Username   string
	UserID     int
	UserRoles  []string
	UserAvatar string
}
