package mixer

type Reply struct {
	T     string `json:"type"`
	ID    int    `json:"id"`
	Error *string

	Data *map[string]interface{}
}

type AuthReply struct {
	Reply
	Data struct {
	}
}
