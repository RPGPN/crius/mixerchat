package mixer

type Event struct {
	T     string `json:"type"`
	Event string `json:"event"`
	Data  struct {
	} `json:"data"`
}

// This will be converted internally to a Message (& co)
type MessageEvent struct {
	Event
	Data struct {
		Channel   int    `json:"channel"`
		Id        string `json:"id"`
		UserName  string `json:"user_name"`
		UserId    int    `json:"user_id"`
		UserLevel int    `json:"user_level"`
		Message   struct {
			Message []struct {
				T           string  `json:"type"`
				Text        string  `json:"text"`
				Data        *string `json:"data,omitempty"`
				URL         *string `json:"url,omitempty"`
				EmoteSource *string `json:"source,omitempty"`
				EmotePack   *string `json:"pack,omitempty"`
				EmoteCoords *struct {
					X      int `json:"x,omitempty"`
					Y      int `json:"y,omitempty"`
					Width  int `json:"width,omitempty"`
					Height int `json:"height,omitempty"`
				} `json:"coords,omitempty"`
			} `json:"message"`
		} `json:"message"`
		Target *string `json:"target,omitempty"`
	} `json:"data"`
}

type WelcomeEvent struct {
	T     string `json:"type"`
	Event string `json:"event"`
	Data  struct {
		Server string `json:"server"`
	} `json:"data"`
}
