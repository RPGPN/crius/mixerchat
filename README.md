# Mixer Chat (for Go)

This isn't a wonderful lib, the use of channels probably isn't correct, and it only panics because of simultaneous map writes occasionally

## Usage
1. Import the library.

2. Get a client id and token - I'm not completely sure how to get these, but I'll update this when I do.

3. Init the library
```go
token := "TOKEN"
clientID := "ClientID"
as := 1234567
mixer := mixerchat.New(token, clientID, as)
```

4. Add a handler for messages
```go
mixer.AddEventHandler(func(conn *mixerchat.Connection, m *mixerchat.Message) {
    // Your code here, maybe you want commands?
})
```

5. Connect to one channel
```go
mixer.Connect(1111111)
```

Or to many
```go
mixer.ConnectMany([]int{1111111,2222222})
```

## Events
### OnMessageCreate

### OnError

### OnPurge
NOT IMPLEMENTED

### OnMessageDelete
NOT IMPLEMENTED