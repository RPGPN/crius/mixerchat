package mixer

type MessageMetaSkill struct {
	SkillID   string
	SkillName string
	ExecID    string
	IconURL   string
	Cost      int
	Currency  string
}

type MessageMeta struct {
	Me       bool
	Censored bool
	Whisper  bool
	IsSkill  bool
	Skill    *MessageMetaSkill
}

type Message struct {
	ID      string
	Channel int
	Author  *User
	Meta    *MessageMeta
	Text    string
}

func NewMessage(from *MessageEvent) *Message {
	var text string
	for _, msg := range from.Data.Message.Message {
		text += msg.Text
	}

	return &Message{
		ID:      from.Data.Id,
		Channel: from.Data.Channel,
		Text:    text,
		Author: &User{
			UserAvatar: "",
			UserID:     from.Data.UserId,
			Username:   from.Data.UserName,
			//UserRoles: from.Data.
		},
	}
}
