package mixer

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

func methodNameFromInt(i int) string {
	switch i {
	case M_AUTH:
		return "AUTH"
	case M_MSG:
		return "MSG"
	case M_WHISPER:
		return "WHISPER"
	case M_PING:
		return "PING"
	default:
		return ""
	}
}

func methodIdFromName(n string) int {
	switch n {
	case "AUTH":
		return M_AUTH
	case "MSG":
		return M_MSG
	case "WHISPER":
		return M_WHISPER
	case "PING":
		return M_PING
	default:
		return -1
	}
}

func (s *Session) Close() {
	close(s.x)
}

func (s *Session) Connect(channelId int) error {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/chats/%d", REST_API, channelId), nil)
	if err != nil {
		return err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", s.token))

	resp, err := s.httpClient.Do(req)
	if err != nil {
		return err
	}

	buf, err := ioutil.ReadAll(resp.Body)

	var data ConnectionJSON

	err = json.Unmarshal(buf, &data)
	if err != nil {
		return err
	}

	mixerErr := data.Error
	if mixerErr != nil {
		panic(data.Message)
	}

	err = resp.Body.Close()
	if err != nil {
		return err
	}

	wsHeader := http.Header{}
	wsHeader.Set("Client-ID", s.clientID)

	wsConn, _, wsErr := websocket.DefaultDialer.Dial(data.Endpoints[0], wsHeader)
	if wsErr != nil {
		return wsErr
	}

	conn := &Connection{
		endpoints:   data.Endpoints,
		permissions: data.Permissions,
		authKey:     *data.AuthKey,
		isLoadShed:  *data.IsLoadShed,
		roles:       data.Roles,
		As:          s.connectAs,
		sock: &ConnectionSock{
			Conn:    wsConn,
			Counter: 0,
			Chain:   make(map[int]int),
		},
	}

	//s.channels[channelId] = conn

	m, err := conn.readMessage()
	if err != nil {
		return err
	}

	var welcomeEvent WelcomeEvent

	jsonErr := json.Unmarshal(m, &welcomeEvent)
	if jsonErr != nil {
		return jsonErr
	}

	logrus.Info(fmt.Sprintf("Connected to %d", channelId))

	err = conn.writeMessage("auth", []interface{}{channelId, 6999442, *data.AuthKey})

	if err != nil {
		return err
	}

	_, m, err = wsConn.ReadMessage()
	if err != nil {
		return err
	}

	err = s.handleReply(conn, m)

	conn.listen(s, s.x)
	return nil
}

func (s *Session) ConnectMany(id []interface{}) error {
	for _, channelId := range id {
		switch k := channelId.(type) {
		case string:
			i, err := strconv.Atoi(k)
			if err != nil {
				return err
			}
			go s.Connect(i)
		case int:
		case int32:
		case int64:
			go s.Connect(int(k))
		default:
			return errors.New("unknown type")
		}
		time.Sleep(1)
	}

	return nil
}

func (s *Session) AddEventHandler(handler interface{}) {
	switch h := handler.(type) {
	case func(*Connection, *Message):
		logrus.Trace("Registered handler onMessageCreate")
		s.onMessageCreate = h
	case func(*Connection, string):
		s.onMessageDelete = h
	case func(*Connection, int):
		s.onPurge = h
	case func(error):
		s.onError = h
	}
}

func New(token string, clientID string, as int) *Session {
	client := &http.Client{
		Timeout: time.Second * 10,
	}

	return &Session{
		token:      token,
		httpClient: client,
		//channels:   make(map[int]*Connection),
		clientID:  clientID,
		x:         make(chan interface{}),
		connectAs: as,
	}
}
